﻿using EFQCDataAccessLib.DataAccess;
using Microsoft.AspNetCore.Mvc;
using qwik_clix_api.ViewModals.PersonVm;

namespace qwik_clix_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class DashboardController : ControllerBase
    {
        private readonly PeopleContext _db;

        public DashboardController(PeopleContext db)
        {
            _db = db;
        }

        // GET: api/Dashboard
        [HttpGet]
        [Route("api/dashboard/index")]
        public Index Index()
        {
            using (_db)
            {
                var people = new Index(_db);
                return people;
            }
        }

        // GET: api/Dashboard/5
        [HttpGet("{id}", Name = "Get")]
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Dashboard
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/Dashboard/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}