﻿using EFQCDataAccessLib;
using EFQCDataAccessLib.DataAccess;
using EFQCDataAccessLib.Modal;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace qwik_clix_api.ViewModals.PersonVm
{
    public class Index : BasePerson
    {
        public int Id { get; set; }
        public List<Index> People { get; set; }

        public Index()
        {
        }

        public Index(PeopleContext _db)
        {
            LoadSampleData(_db);
            var people = _db.People
               .Include(a => a.Addresses)
               .Include(e => e.EmailAddresses)
               .ToList();
        }

        private void LoadSampleData(PeopleContext _db)
        {
            if (_db.People.Count() == 0)
            {
                var mockPeople = new List<Person>
                {
                    new Person
                    {
                        Id = 1,
                        FirstName = "Mike",
                        Surname = "Smith",
                        Age=39,
                        Addresses = new List<Address>{
                            new Address {
                                Id=1,
                                Street= "129 Stenly Str",
                                City= "Centurion",
                                Province= "Gauteng",
                                Code= "0102"
                            }
                        },
                        EmailAddresses = new List<Email>
                        {
                            new Email
                            {
                                Id = 1,
                                EmailAddress="m.smith@qwikclix.com"
                            }
                        }
                    },
                    new Person
                    {
                        Id = 2,
                        FirstName = "Jill",
                        Surname = "Brown",
                        Age=25,
                        Addresses = new List<Address>{
                            new Address {
                                Id=2,
                                Street= "89 Glenwood Str",
                                City= "Menlyn",
                                Province= "Gauteng",
                                Code= "0102"
                            }
                        },
                        EmailAddresses = new List<Email>
                        {
                            new Email
                            {
                                Id = 2,
                                EmailAddress="j.brown@qwikclix.com"
                            }
                        }
                    }
                };

                _db.AddRange(mockPeople);
                _db.SaveChanges();
            }
        }
    }
}