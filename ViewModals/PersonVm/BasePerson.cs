﻿using System.ComponentModel.DataAnnotations;

namespace qwik_clix_api.ViewModals.PersonVm
{
    public abstract class BasePerson
    {
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Display(Name = "Surname")]
        public string Surname { get; set; }

        [Display(Name = "Age")]
        public int Age { get; set; }
    }
}